## What is in this repository?
An **Xcode application** built in **swift 5.0**, fetches the latest block from the block chain and display its block number on the tableview. Currently it is displaying 20 latest blocks. Newer block added on the top and pushes down the old with native tableview animation. User can tap on the block number and see the `Producer Info`, `Transaction count`, `Producer Signature` and `JSON Response` which can be hide/unhide using native switch control.

---

## Quick Start

**Design Pattern**
This application utilizing **MVVM** design pattern.

There are **three controllers** 

1. `ViewController` - **Which displays only one button in the center of the screen**

2. `BlocksViewController` - **Controller shows list of blocks fetched from Public Blockchain Info API**

3. `BlockInfoViewController` - **This controller displays details `Producer Info`, `Transaction count`, `Producer Signature` and `JSON Response` of the block number selected on `BlocksViewController` page**

There are **ServiceLayer Layer** 
This is complete seperation of API logic from UI layer. This utility is creating and execute `URLRequest`, respond back to caller using `closure`

There are **Unit Tests** 
There are two unit test files added with respect to 2 main controllers and tried to cover as much test as possible, listing them below

1. 'BlocksViewControllerTests' - XCTest to test **`BlocksViewController`**

2. 'BlockInfoViewControllerTests' - XCTest to test **`BlockInfoViewController`**

---

## Screenshots

![video](https://bitbucket.org/sam091119/codingassignment/src/master/Screenshots/BlockOne_CodingAssignment.mov)

![icon](https://bitbucket.org/sam091119/codingassignment/raw/69edf07cb411a016cd91e102e91429bbc854891b/Screenshots/1.Screenshot.png)

![splash screen](https://bitbucket.org/sam091119/codingassignment/raw/69edf07cb411a016cd91e102e91429bbc854891b/Screenshots/2.Screenshot.png)

![Block Button Page](https://bitbucket.org/sam091119/codingassignment/raw/69edf07cb411a016cd91e102e91429bbc854891b/Screenshots/3.Screenshot.png)

![Blocks List](https://bitbucket.org/sam091119/codingassignment/raw/69edf07cb411a016cd91e102e91429bbc854891b/Screenshots/4.Screenshot.png)

![Blocks Detail Page](https://bitbucket.org/sam091119/codingassignment/raw/69edf07cb411a016cd91e102e91429bbc854891b/Screenshots/5.Screenshot.png)

![Blocks Detail with Raw JSON](https://bitbucket.org/sam091119/codingassignment/raw/69edf07cb411a016cd91e102e91429bbc854891b/Screenshots/6.Screenshot.png)








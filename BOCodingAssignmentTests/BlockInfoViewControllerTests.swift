//
//  BOCodingAssignmentTests.swift
//  BOCodingAssignmentTests
//
//  Created by Sanjeev Bharti on 9/9/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import XCTest
@testable import BOCodingAssignment

class BlockInfoViewControllerTests: XCTestCase {
    
    var viewController: BlockInfoViewController?
    
    override func setUp() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        viewController = storyBoard.instantiateViewController(withIdentifier: "BlockInfoViewController") as? BlockInfoViewController
        viewController?.loadView()
    }
    
    override func tearDown() {
        viewController = nil
    }
    
    func testInitializations() {
        XCTAssertNotNil(viewController, "After initialization viewController should not be nil")
        XCTAssertNotNil(viewController?.view, "After initialization view should not be nil")
        XCTAssertNotNil(viewController?.viewModel, "After initialization view should not be nil")
        XCTAssertNotNil(viewController?.title, "title should not be nil")
    }
    
    func testConfigureUI() {
        viewController?.viewModel.blockId = 123123123
        viewController?.configureUI()
        XCTAssertTrue(viewController?.viewModel.activityIndicator.style == .gray,
                      "Activity Indicator should be gray after configureUI function call")
        
        XCTAssertEqual((viewController?.title ?? "") ,
                       "123123123",
                       "Activity Indicator should be 123123123 after configureUI function call")
    }
    
    func testIBOutlets() {
        XCTAssertNotNil(viewController?.producerLabel, "producerLabel should not be nil")
        XCTAssertNotNil(viewController?.signatureLabel, "signatureLabel should not be nil")
        XCTAssertNotNil(viewController?.transactionCountLabel, "transactionCountLabel should not be nil")
        XCTAssertNotNil(viewController?.jsonResponseTextView, "jsonResponseTextView should not be nil")
    }
    
    func testViewModelProperties() {
        guard let viewModel = viewController?.viewModel else {return}
        XCTAssertNil(viewModel.blockId, "Before initialization blockId should be nil")
        XCTAssertNil(viewModel.block, "Before initialization block should be nil")
        XCTAssertNil(viewModel.data, "Before initialization data should be nil")
        
        XCTAssertEqual((viewModel.jsonResponse?.count ?? 0), 0, "jsonResponse text count should be 0")
        
        viewModel.blockId = 123423
        XCTAssertNotNil(viewModel.blockId, "After initialization blockId should not be nil")
        
        viewModel.block = Block.mockData()
        XCTAssertNotNil(viewModel.block, "After initialization block should not be nil")
        
        viewModel.data = Data()
        XCTAssertNotNil(viewModel.data, "After initialization data should not be nil")
        XCTAssertNotNil(viewModel.jsonResponse, "jsonResponse initialization data should not be nil")
    }
    
}

//
//  BlocksViewControllerTests.swift
//  BOCodingAssignmentTests
//
//  Created by Sanjeev Bharti on 9/11/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import XCTest
@testable import BOCodingAssignment

class BlocksViewControllerTests: XCTestCase {
    
    var viewController: BlocksViewController?
    
    override func setUp() {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        viewController = storyBoard.instantiateViewController(withIdentifier: "BlocksViewController") as? BlocksViewController
        viewController?.loadView()
    }
    
    override func tearDown() {
        viewController = nil
    }
    
    func testInitializations() {
        XCTAssertNotNil(viewController, "After initialization viewController should not be nil")
        XCTAssertNotNil(viewController?.view, "After initialization view should not be nil")
        XCTAssertNotNil(viewController?.viewModel, "After initialization view should not be nil")
        XCTAssertNotNil(viewController?.counterLabel, "Table footview should not be nil")
        XCTAssertNotNil(viewController?.title, "title should not be nil")
    }
    
    func testTableViewDataSource() {
        XCTAssertNotNil(viewController?.tableView.dataSource, "Tableview dataSource should not be nil")

        XCTAssertTrue(viewController?.tableView.dataSource is BlocksViewController, "Tableview dataSource should conform to BlocksViewController")
    }
   
    func testTableViewDelegate() {
        XCTAssertNotNil(viewController?.tableView.delegate, "Tableview delegate should not be nil")
        XCTAssertTrue(viewController?.tableView.delegate is BlocksViewController, "Tableview delegate should conform to BlocksViewController")
    }
    
    func testViewModelProperties() {
        XCTAssertNotNil(viewController?.viewModel.activityIndicator, "after initialization activity indicator should not be nil")
        XCTAssertNotNil(viewController?.viewModel.blocks, "blocks should not be empty")
    }

    func testConfigureUI() {
        
        viewController?.configureUI()
        XCTAssertNotNil(viewController?.tableView.tableFooterView, "table footview should not be nil")
        XCTAssertEqual(viewController?.counterLabel.text ?? "", "Block 0 of 20", "counter label initial text should be Block 0 of 20")
        XCTAssertTrue(viewController?.viewModel.activityIndicator.style == .gray)
        XCTAssertNotNil(viewController?.navigationItem.rightBarButtonItem, "after initialization activity indicator should not be nil")
    }
    
    func testBlockChainInfoAPI() {
        XCTAssertTrue((viewController?.viewModel.blocks.count ?? 0) == 0, "Initial blocks count should be 0")
        viewController?.viewModel.retriveBlockChainInfoWith(completionHandler: { (error) in
            XCTAssertTrue((self.viewController?.viewModel.blocks.count ?? 0) > 0, "Blocks count should be greater than 0")
        })
    }
    
}

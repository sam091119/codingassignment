//
//  BlocksViewController.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/9/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import UIKit

class BlocksViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var counterLabel: UILabel!
    
    //MARK:- Properties
    var viewModel = BlocksViewModel()
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        retriveChainInfo()
    }
    
    //MARK:- Methods
    
    func configureUI() {
        // remove the unnecessary rows from the bottom of tableview
        tableView.tableFooterView = UIView()
        updateCounterLabel()
        addActivityIndicator()
    }
    
    private func updateCounterLabel() {
        counterLabel.text = "Block \(viewModel.blocks.count) of \(BlocksViewModelConstant.blocksLimit)"
    }

    private func addActivityIndicator() {
        viewModel.activityIndicator.style = .gray
        let barButton = UIBarButtonItem(customView: viewModel.activityIndicator)
        navigationItem.setRightBarButton(barButton, animated: true)
        viewModel.activityIndicator.startAnimating()
    }
    
    private func retriveChainInfo() {
        viewModel.retriveBlockChainInfoWith {[weak self] error in
            
            if error != nil {
                self?.display(error)
                return
            }
            
            DispatchQueue.main.async {
                self?.updateCounterLabel()
                if self?.viewModel.blocks.count == BlocksViewModelConstant.blocksLimit {
                    self?.viewModel.activityIndicator.stopAnimating()
                }
                self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }
        }
    }
    
    private func display(_ error: Error?) {
        guard let error = error else {return}
        
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok",
                                   style: .default,
                                   handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            alert.show(self, sender: nil)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {return}
        
        switch identifier {
        case BlocksViewModelConstant.Identifiers.blockInfo:
            if let indexPath = tableView.indexPathForSelectedRow, let blockInfoVC = segue.destination as? BlockInfoViewController {
                let selectedBlockNumber = viewModel.blocks[indexPath.row]
                blockInfoVC.viewModel.blockId = selectedBlockNumber
            }
        default: break
        }
        
    }
    
    
}

extension BlocksViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: BlocksViewModelConstant.Identifiers.blockInfo,
                          sender: self)
    }
}

extension BlocksViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.blocks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BlocksViewModelConstant.CellIdentifiers.blockInfo, for: indexPath) as? BlockInfoTableCell
        cell?.configure(text: "\(viewModel.blocks[indexPath.row])")
        return cell ?? UITableViewCell()
    }
    
    
}


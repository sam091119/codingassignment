//
//  BlockInfoViewController.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/11/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import UIKit

class BlockInfoViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet var producerLabel: UILabel!
    @IBOutlet var transactionCountLabel: UILabel!
    @IBOutlet var signatureLabel: UILabel!
    @IBOutlet var jsonResponseTextView: UITextView!
    
    // MARK: - Properties
    var viewModel = BlockInfoViewModel()
    
    // MARK: - View Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchBlockInfo()
    }
    
    // MARK: - Methods

    func configureUI() {
        if let blockID = viewModel.blockId {
            title = "\(blockID)"
        }
        addActivityIndicator()
    }
    
    private func addActivityIndicator() {
        viewModel.activityIndicator.style = .gray
        let barButton = UIBarButtonItem(customView: viewModel.activityIndicator)
        navigationItem.setRightBarButton(barButton, animated: true)
        viewModel.activityIndicator.startAnimating()
    }
    
    private func fetchBlockInfo() {
        guard let blockId = viewModel.blockId else {return}
        viewModel.retrieveBlockDetailsWith(id: blockId) {[weak self] (error) in
            DispatchQueue.main.async {
                self?.viewModel.activityIndicator.stopAnimating()
            }
            
            if error != nil {
                self?.display(error)
                return
            }
            
            guard let block = self?.viewModel.block else {return}
            DispatchQueue.main.async {
                self?.producerLabel.text = block.producer
                self?.signatureLabel.text = block.producer_signature
                self?.transactionCountLabel.text = "\(block.transactions?.count ?? 0)"
            }
        }
    }
    
    private func display(_ error: Error?) {
        guard let error = error else {return}
        
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok",
                                   style: .default,
                                   handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            alert.show(self, sender: nil)
        }
    }
    
    // MARK: - Action Methods

    @IBAction func toggle(sender: UISwitch) {
        jsonResponseTextView.text = sender.isOn ? self.viewModel.jsonResponse : ""
    }
    
}

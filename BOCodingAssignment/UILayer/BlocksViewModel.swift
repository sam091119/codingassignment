//
//  BlocksViewModel.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/9/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import UIKit

struct BlocksViewModelConstant {
    static let blocksLimit = 20
    struct Identifiers {
        static let blockInfo = "displayBlockInformation"
    }
    struct CellIdentifiers {
        static let blockInfo = "BlockInfoTableCell"
    }
}

class BlocksViewModel {
    
    var activityIndicator = UIActivityIndicatorView()
    var blocks:[Int] = []
    
    // MARK:- API
    
    func retriveBlockChainInfoWith(completionHandler: @escaping CompletionHandlerWithError) {
        ServiceManager.retrieveBlockChainInfoWith { (data, urlResponse, error) in
            
            guard let data = data else {return}
            
            do {
                let chainInfo = try JSONDecoder().decode(ChainInfo.self, from: data)
                
                // only unique blocks
                if !self.blocks.contains(chainInfo.head_block_num) {
                    self.blocks.insert(chainInfo.head_block_num, at: 0)
                    completionHandler(error)
                }
                
                if self.blocks.count < BlocksViewModelConstant.blocksLimit {
                    self.retriveBlockChainInfoWith(completionHandler: completionHandler)
                }
            } catch {
                completionHandler(error)
            }
            
        }
    }
    
}

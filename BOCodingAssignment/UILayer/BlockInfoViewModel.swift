//
//  BlockInfoViewModel.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/11/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import UIKit

class BlockInfoViewModel {
    
    // MARK: - Properties
    var blockId: Int?
    var block: Block?
    var data: Data?
    
    var jsonResponse: String? {
        guard let responseData = data else {return ""}
        return String(bytes: responseData, encoding:   String.Encoding.utf8)
    }
    
    var activityIndicator = UIActivityIndicatorView()

    // MARK: - API Calls
    
    func retrieveBlockDetailsWith(id: Int, completionHandler: @escaping CompletionHandlerWithError) {
        
        ServiceManager.retrieveBlockDetailWith(id: id) { (data, response, error) in
            guard let data = data else {return}
            do {
                self.block = try JSONDecoder().decode(Block.self, from: data)
                self.data = data
                completionHandler(error)
            } catch {
                print("Error Occured while decoding data")
                completionHandler(error)
            }
            
        }
    }
    
}

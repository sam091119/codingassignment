//
//  ServiceType.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import UIKit

enum Service: String {
    // get
    case blockChainInfo = "blockChainInfo"
    case blockDetails = "blockDetails"
    
}

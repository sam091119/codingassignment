//
//  RemoteDataManager.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

struct UrlRequestInfo {
    var service:Service
    var baseUrl :String
    var endpoint :String
    var httpType :HTTPMethodType
}

struct ServiceEndPoint {
    static let blockChainInfo = "v1/chain/get_info"
    static let retrieveBlockDetails = "v1/chain/get_block"
}

struct UrlInfoManager {
 
    private static let baseURL = "https://api.eosnewyork.io/"
    
    static func retreive(key:Service) -> UrlRequestInfo?  {
        
        var urlRequestInfo: UrlRequestInfo?
        
        switch key {
        
        case .blockChainInfo:
            urlRequestInfo = createRetriveBlockRequest()
        
        case .blockDetails:
            urlRequestInfo = createRetriveBlockDetail()
        }
        
        return urlRequestInfo
    }
    
    static func createRetriveBlockRequest() -> UrlRequestInfo {
        
        let request = UrlRequestInfo(service: .blockChainInfo,
                                     baseUrl: baseURL,
                                     endpoint: ServiceEndPoint.blockChainInfo,
                                     httpType: .post)
        return request
    }
    
    static func createRetriveBlockDetail() -> UrlRequestInfo {
        
        let request = UrlRequestInfo(service: .blockDetails,
                                     baseUrl: baseURL,
                                     endpoint: ServiceEndPoint.retrieveBlockDetails,
                                     httpType: .post)
        return request
    }
    
}

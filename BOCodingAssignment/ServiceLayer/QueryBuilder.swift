//
//  QueryBuilder.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

enum QueryType {
    case forwardSlash
    case questionMark
    case messageBody
}

struct QueryBuilder {
    
    static func constructUrlRequest(for service:Service,
                                    queryType: QueryType,
                                    params:[String : Any]? = [:],
                                    components:[Any]? = nil) -> URLRequest? {
        
        guard let info :UrlRequestInfo = UrlInfoManager.retreive(key:service) else {return nil}
        let url = info.baseUrl + info.endpoint
        var serviceUrl = URL.init(string: url)
        
        var request = URLRequest.init(url: serviceUrl!)
        request.httpMethod = info.httpType.rawValue
        
        switch queryType {
        case .forwardSlash:
            
            //Add components like /searchQuery/1/100
            if let components = components {
                for comp in components {
                    // Encoding done automatically.
                    serviceUrl?.appendPathComponent("\(comp)")
                }
            }
            request.url = serviceUrl
            
        case .questionMark:
            
            guard let params = params else {return nil}
            
            var queryItems:[URLQueryItem] = []
            for key in params.keys {
                let queryItem = URLQueryItem(name: key, value: params[key] as? String)
                queryItems.append(queryItem)
            }
            
            var urlComponent = URLComponents(url: request.url!, resolvingAgainstBaseURL: false)
            urlComponent?.queryItems = queryItems
            request.url = urlComponent?.url
            
        case .messageBody:
            if let params = params {
                guard let httpBody = try? JSONSerialization.data(withJSONObject: params as Any, options: []) else {
                    return nil
                }
                request.httpBody = httpBody
            }
        
        }
        
        if var requestString = request.url?.absoluteString, requestString.contains("%3F") {
            requestString = requestString.replacingOccurrences(of: "%3F", with: "?")
            request.url = URL(string: requestString)
        }
        
        return request
    }
    
}

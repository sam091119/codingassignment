//
//  Handlers.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

typealias EmptyHandler = () -> Void
typealias CompletionHandlerWithError = (Error?) -> Void
typealias ServiceResponseHandler = (Data?, URLResponse?, Error?) -> Void


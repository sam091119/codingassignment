//
//  ServiceManager.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

class ServiceManager {
    
    static func retrieveBlockChainInfoWith(compltionHandler: @escaping ServiceResponseHandler) {
        RemoteDataManager.retrieveBlockWith(completionHandler: compltionHandler)
    }
    
    static func retrieveBlockDetailWith(id: Int, completionHandler: @escaping ServiceResponseHandler) {
        RemoteDataManager.retrieveBlockDetailWith(id: id,
                                                  completionHandler: completionHandler)
    }
    
}

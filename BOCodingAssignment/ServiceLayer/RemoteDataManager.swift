//
//  RemoteDataManager.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

class RemoteDataManager {
    
    static func retrieveBlockWith(completionHandler: @escaping ServiceResponseHandler) {

        let request = QueryBuilder.constructUrlRequest(for: .blockChainInfo, queryType: .forwardSlash, params: nil, components: nil)
        guard let urlRequest = request else {
            return }
        ServiceExecutioner.execute(request: urlRequest, completionHandler: completionHandler)
    }
    
    static func retrieveBlockDetailWith(id: Int, completionHandler: @escaping ServiceResponseHandler) {
        
            let params = ["block_num_or_id" : id]
            let request = QueryBuilder.constructUrlRequest(for: .blockDetails, queryType: .messageBody, params: params, components: nil)
            guard let urlRequest = request else {return}
            
            ServiceExecutioner.execute(request: urlRequest, completionHandler: completionHandler)
    }
    
}

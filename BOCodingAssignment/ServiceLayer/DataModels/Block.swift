//
//  block.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

class Block: Codable {

    let timestamp: String
    let producer: String
    let confirmed: Int?
    let previous: String?
    let transaction_mroot: String?
    let action_mroot: String?
    let schedule_version: Int?
    let new_producers: String?
    let header_extensions: [String?]?
    let producer_signature: String
    let block_extensions: [String?]?
    
    let id: String
    let block_num: Int
    let ref_block_prefix: Int?
    let transactions:[Transaction?]?
    
    init(timestamp: String,
         producer: String,
         confirmed: Int? = nil,
         previous: String? = nil,
         transaction_mroot: String? = nil,
         action_mroot: String? = nil,
         schedule_version: Int? = nil,
         new_producers: String? = nil,
         header_extensions: [String?]? = nil,
         producer_signature: String,
         block_extensions: [String?]? = nil,
         id: String,
         block_num: Int,
         ref_block_prefix: Int? = nil,
         transactions:[Transaction?]? = nil) {
        
        self.timestamp = timestamp
        self.producer = producer
        self.confirmed = confirmed
        self.previous = previous
        self.transaction_mroot = transaction_mroot
        self.action_mroot = action_mroot
        self.schedule_version = schedule_version
        self.new_producers = new_producers
        self.header_extensions = header_extensions
        self.producer_signature = producer_signature
        self.block_extensions = block_extensions
        self.id = id
        self.block_num = block_num
        self.ref_block_prefix = ref_block_prefix
        self.transactions = transactions
    }
}

class Transaction: Codable {
    let status: String?
    let cpu_usage_us: Int?
    let net_usage_words: Int?
}

class Trx: Codable {
    let id: String?
    let signatures: [String?]?
    let compression: String?
    let packed_context_free_data: String?
    let context_free_data: [String?]?
    let packed_trx: String?
}

extension Block {
    
    static func mockData() -> Block {
       return Block(timestamp: "test_time_stamp",
                    producer: "eso.io",
                    producer_signature: "test producer signature",
                    id: "test_id",
                    block_num: 123123123)
    }
}

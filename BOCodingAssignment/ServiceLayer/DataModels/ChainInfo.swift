//
//  ChainInfo.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

/*
 "server_version":"14185431",
 "chain_id":"aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906",
 "head_block_num":78706834,
 "last_irreversible_block_num":78706505,
 "last_irreversible_block_id":"04b0f749bce0e8ec7535cf7921f79b53fed32eb71019cb4d36e77f77ebc8d86c",
 "head_block_id":"04b0f892b1ebbfb13ae61c1240661747ad6132863a8496a2430a044728248f7b",
 "head_block_time":"2019-09-10T22:16:39.000",
 "head_block_producer":"eosrapidprod",
 "virtual_block_cpu_limit":200000000,
 "virtual_block_net_limit":1048576000,
 "block_cpu_limit":121554,
 "block_net_limit":1035720,
 "server_version_string":"v1.8.1",
 "fork_db_head_block_num":78706834,
 "fork_db_head_block_id":"04b0f892b1ebbfb13ae61c1240661747ad6132863a8496a2430a044728248f7b"
 */

class ChainInfo: Codable {
    let server_version: String?
    let chain_id: String
    let head_block_num: Int
    let last_irreversible_block_num: Int?
    let last_irreversible_block_id: String?
    let head_block_id: String?
    let head_block_time: String?
    let head_block_producer: String?
    let virtual_block_cpu_limit: Int?
    let virtual_block_net_limit: Int?
    let block_cpu_limit: Int?
    let block_net_limit: Int?
    let server_version_string: String?
    let fork_db_head_block_num: Int?
    let fork_db_head_block_id: String?
}



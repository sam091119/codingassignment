//
//  ServiceExecutioner.swift
//  BOCodingAssignment
//
//  Created by Sanjeev Bharti on 9/10/19.
//  Copyright © 2019 sanjeev bharti. All rights reserved.
//

import Foundation

class ServiceExecutioner {
    
    static func execute(request: URLRequest, completionHandler: @escaping ServiceResponseHandler) {
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            completionHandler(data, response, error)
            }.resume()
    }
}
